# IMIE

# @author     GauthierMNTS
# @package    imie\automatisation\python
# @subpackage objectHangmanGame.py
# @version    1.0.0 20180426 GauthierMNTS Intial script

# Import

import os, random, csv

# Game Class

class HangmanGame:

    # Constructor
    
    def __init__(self, dictionnary, essay):
        self.dictionnary = dictionnary
        self.essay = essay
        self.essayBase = self.essay
        
        self.Run = True
        self.App()

    # Function

    def App(self):
        print(" *-| Jeu du pendu |-*\n----------------------\n")
        self.userName = input("Quel est votre pseudo ? ")
        
        while self.Run:
            self.generateGame()
            
            while True:
                print("Vous avez ", str(self.essay), " essai(s).")
                print("\nLe mot est ", self.wordCrypt)
                self.userLetter = input("Entrez une lettre :> ")
                self.positionLetter = self.givePositionLetter(self.userLetter.lower(), self.wordLetter)

                if len(self.positionLetter) == 0:
                    print("\nLa lettre ne correspond pas.\n")
                    self.essay -= 1
                else:
                    print("\n")
                    self.wordCrypt = self.replaceLetter(self.word, self.wordCrypt, self.positionLetter)

                if self.compareWordAndCrypt(self.word, self.wordCrypt):
                    print("Vous avez trouvez le mot!")
                    print(self.word)
                    newGame = input("\nVoulez-vous rejouer ? (y = continuer / entrée = quitter) :")
                    askToContinue = True
                    break

                if self.essay <= 0:
                    print("\nVous avez perdu!")
                    print("Le mot était : ", self.word)
                    newGame = input("\nVoulez-vous rejouer ? (y = continuer  / entrée = quitter) :")
                    askToContinue = True
                    break
            
            if askToContinue:
                if newGame == None or newGame != 'y':
                    self.Run = False
                else:
                    print("\n------------------------------\nDémarrage d'une nouvelle partie !")
                    self.essay = self.essayBase
        
        print("Au revoir", self.userName, "!")

    # Initialisation des donnée de la partie
    def generateGame(self):
        self.wordList = self.extractWordToFile(self.dictionnary)
        self.word = self.chooseWord(self.wordList)
        self.wordLetter = self.enumerateLetter(self.word)
        self.wordCrypt = self.cryptWord(self.word)
    
    # Extraction des mots du dictionnaire
    # @param  path  string Chemin vers le fichier contenant les mots
    # @return array        Tous les mots du dictionnaire
    def extractWordToFile(self, path):
            file = open(path)
            return file.read().splitlines()

    # Choisi un mot au hasard
    # @return string Un mot
    def chooseWord(self, content):
            return random.choice(content)

    # Création d'un tableau de tuple des lettres que contient le mot
    # @param  word string      Mot choisi
    # @return      array/tuple Tableau associatif des positions de chaque lettre dans le mot
    def enumerateLetter(self, word):
            return list(enumerate(word))

    # Création du mot crypté
    # @param  word string Mot choisi
    # @return      string Mot crypté
    def cryptWord(self, word):
            return "*" * len(word)

    # Donne les toutes les positions de lettre choisi dans le mot
    # @param  letter string Lettre de l'utilisateur
    # @param  word   string Mot choisi
    # @return        array  Tableau contenant la/les positions des lettres dans le mots
    def givePositionLetter(self, letter, word):
            return [x for x, y in word if y == letter]

    # Remplace les caractères cryptés correspondants à la lettre trouvée
    # @param  word      string Mot choisi
    # @param  wordCrypt string Mot crypté
    # @param  position  array  Position des lettres trouvés
    # @return           string Mot crypté avec lettres découvertes
    def replaceLetter(self, word, wordCrypt, position):
            l = list(wordCrypt)
            for i in position:
                    l[i] = word[i]
            return "".join(l)

    # Vérifie si le mot choisi a été trouvé
    # @param  word      string Mot choisi
    # @param  wordCrypt string Mot crypté possiblement trouvé
    # @return           bool
    def compareWordAndCrypt(self, word, wordCrypt):
            if word == wordCrypt:
                    return True
            else:
                    return False

HangmanGame("wordList.txt", 15)